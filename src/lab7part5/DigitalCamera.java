/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7part5;

/**
 *
 * @author Admin
 */
public abstract class DigitalCamera {

  private String Make;
    private String Model;
      private double Megapixel;
      private double Internal;
            private double External;
      

  public DigitalCamera(String Make,String Model,double Megapixel,double Internal,double External ) {

    this.Make = Make;
     this.Model = Model;
      this.Megapixel = Megapixel;
 
        this.External = External;
      
     
    
  }



  // Provide an implementation for inherited abstract getArea() method
  public String getMake() {
    return Make;
  }
  public String getModel() {
    return Model;
  }
    public double getMegapixel() {
    return Megapixel;
  }
  // Provide an implementation for inherited abstract getPerimeter() method

    public double getExternal() {
    return External;
  }
      public String getDescribeCam() {
    return "make : " + Make + "/n" + "Model : " + Model + "/n" + "Megapixel : " + Megapixel  + " External Storage : " + External;
  }
    
    
}
